<!--
CSCI 466/566 - Assignment 9,10  - Semester (Fall) 2016
File:           index.php
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Jon Lehuta
TA:             Rajeswari Gundu
TA:             Bari Abdul
Date Due:       Monday , November  11, 2016
Purpose:        Php and mysql embeeding

-->
<!DOCTYPE html>
<html>
<head>
	<title>Assignment 10 & 9</title>
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="css/materialize.min.css" media="screen,projection" rel="stylesheet" type="text/css">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
</head>
<body> 

<div class="container">

<pre>
CSCI 466/566 - Assignment 9,10  - Semester (Fall) 2016
File:           index.php
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Jon Lehuta
TA:             Rajeswari Gundu
TA:             Bari Abdul
Date Due:       Friday , November  11, 2016
Purpose:        Mysql Embeeding in PHP 

</pre>

	    <blockquote> <h3>Assignment 10</h3></blockquote>
		<hr>
		<blockquote>Allow the user to add a new movie</blockquote>

<!-- 


-->

 <div class="row">
   	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
				<div class="input-field col s6">
			    <input id="movie_name"  name="movie_name"  type="text" required>
			    <label for="movie_name">Movie Name</label>
			    </div>
				<div class="input-field col s3">
			    <input id="year"  name="year" type="number" required >
			    <label for="year">year</label>
			    </div>
				<div class="input-field col s3">
			    <input id="length"  name="length" type="number" required>
			    <label for="length">length</label>
			    </div>
        </div>
        <div class="row">
				<div class="input-field col s4"></div>
				<div class="input-field col s4">
				 <button class="btn waves-effect waves-light" type="submit" name="movie_submit">Submit
				    <i class="material-icons right">send</i>
  				</button>

		</div>
				<div class="input-field col s4">
				</div>


			</form>
			<?php 
    if ( isset( $_POST['movie_submit'] ) ) {
        // Do processing here.
         $movie_name = $_POST["movie_name"];
         $year = $_POST["year"];
         $length = $_POST["length"];

         $insert = 'insert into movie(title,releaseYear,length) values 
	("'.$movie_name .'", '.$year.','.$length.')';
	 $username = "z1803493";$password = "1995Jul05";
	 $dsn = "mysql:host=Courses;dbname=z1803493";
	            $pdo = new PDO($dsn,$username, $password);
	            
	            $stmt = $pdo->query($insert);

    }
?>

        </div>



		<hr><blockquote> Allow the user to add either a new director or new actor</blockquote>
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">

<!-- 


-->

		<div class="row">
			<div class="input-field col s6">
	          <input name ="first_name" id="first_name" type="text" class="validate" required>
	          <label for="first_name">First Name</label>
	        </div>
			<div class="input-field col s6">
	          <input name ="last_name" id="last_name" type="text" class="validate" required>
	          <label for="last_name">Last Name</label>
	        </div>
		</div>
		<div class="row">
			<div class="input-field col s6">
	          <select name="type" required>
			      <option value="" disabled selected>Choose your option</option>
			      <option value="1">Director</option>
			      <option value="2">Actor</option>
			    </select>
	        </div>
			<div class="input-field col s6">
	          <button class="btn waves-effect waves-light" type="submit" name="person_submit">Submit
    <i class="material-icons right">send</i>
  </button>
	        </div>
		</div>
</form>
<?php 
    if ( isset( $_POST['person_submit'] ) ) {
        // Do processing here.
         $first_name = $_POST["first_name"];
         $last_name = $_POST["last_name"];
         $flag = $_POST["type"];

         if ( $flag == 1) {
         	$insert = 'insert into director(fname,lname) values("'.$first_name.'", "'.$last_name.'")';
         }
         else{
			$insert = 'insert into actor(fname,lname) values("'.$first_name.'", "'.$last_name.'")';
		}

	 $username = "z1803493";$password = "1995Jul05";
	 $dsn = "mysql:host=Courses;dbname=z1803493";
	            $pdo = new PDO($dsn,$username, $password);
	           $stmt = $pdo->query($insert);

    }
?>


		<hr><blockquote> Allow the user to choose a movie and either a director or actor and connect them (make sure you add the salary as well)</blockquote> 
	
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
		
<!-- 


-->

		<div class="row">
		<div class="input-field col s6">
			<?php
	        	$username = "z1803493";
				$password = "1995Jul05";
				$dsn = "mysql:host=Courses;dbname=z1803493";
	            $pdo = new PDO($dsn, $username, $password);
	        	$sql = "select * from movie";
	            $stmt = $pdo->query($sql);
	            $result = $stmt -> fetchAll();
	         ?>
	            <select name="mid" required >
					<option value="" disabled selected>Choose Movie</option>
		         	<?php
		            foreach ( $result as $row ) {
		                echo '<option value="'.$row[mid].'">',$row[title],'</option>';
		            }
					?>
				</select>

		</div>
		<div class="input-field col s6">
			<?php
	        	$username = "z1803493";
				$password = "1995Jul05";
				$dsn = "mysql:host=Courses;dbname=z1803493";
	            $pdo = new PDO($dsn, $username, $password);
	        	$sql = "select * from director";
	            $stmt = $pdo->query($sql);
	            $result = $stmt -> fetchAll();
	         ?>
	            <select name="did" required >
					<option value="" disabled selected>Choose Director</option>
		         	<?php
		            foreach ( $result as $row ) {
		                echo '<option value="'.$row[did].'">',$row[fname],' ',$row[lname],'</option>';
		            }
					?>
				</select>
		</div>	
		</div>
		<div class="row">
		<div class="input-field col s6">
			<input id="salary"  name="salary" type="number" required>
			    <label for="salary">Salary</label>
		</div>
		<div class="input-field col s6">
				<button class="btn waves-effect waves-light" type="submit" name="salary_submit">Submit
    <i class="material-icons right">send</i>
  </button>
		</div>	
		</div>
</form>

<?php 
    if ( isset( $_POST['salary_submit'] ) ) {
        // Do processing here.
         $mid = $_POST["mid"];
         $did = $_POST["did"];
         $salary = $_POST["salary"];
         $insert = 'insert into directs values ('.$mid.','.$did.','.$salary.')';
	 $username = "z1803493";$password = "1995Jul05";
	 $dsn = "mysql:host=Courses;dbname=z1803493";
	            $pdo = new PDO($dsn,$username, $password);
	           $stmt = $pdo->query($insert);

    }
?>


 <hr>
<blockquote> <h3>Extra Credit</h3></blockquote>
<hr>
<blockquote> Allow the user to delete an actor (or director), this means changing all of the possible connections to movies. If the base portion of this assignment does not work, you cannot get extra credit. This must work absolutely correctly, no partial credit.    </blockquote>
	
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
	
<!-- 


-->

	<div class="row">
			
			<div class="input-field col s6">
			<?php
	        	$username = "z1803493";
				$password = "1995Jul05";
				$dsn = "mysql:host=Courses;dbname=z1803493";
	            $pdo = new PDO($dsn, $username, $password);
	        	$sql = "select * from director";
	            $stmt = $pdo->query($sql);
	            $result = $stmt -> fetchAll();
	         ?>
	            <select name="did" required >
					<option value="" disabled selected>Choose Director</option>
		         	<?php
		            foreach ( $result as $row ) {
		                echo '<option value="'.$row[did].'">',$row[fname],' ',$row[lname],'</option>';
		            }
					?>
				</select>
		</div>
			<div class="input-field col s6">
<button class="btn waves-effect waves-light" type="submit" name="delete_submit">Submit
    <i class="material-icons right">send</i>
  </button>
			</div>
			

	</div>
</form>

<?php 
    if ( isset( $_POST['delete_submit'] ) ) {
        
         $did = $_POST["did"];
         
         $q2 = 'delete from director where did = '.$did;
         $q3 = 'delete from directs where did = '.$did;
	 $username = "z1803493";$password = "1995Jul05";
	 $dsn = "mysql:host=Courses;dbname=z1803493";
	            $pdo = new PDO($dsn,$username, $password);

	           $pdo->query($q3);
	            $pdo->query($q2);


    }
?>

		
	    <hr>
<blockquote> <h3>Assignment 9</h3></blockquote>
<hr>
<blockquote> First Question: list all of the movies    </blockquote>

<!-- 


-->

<table class="highlight" class="centered" class="responsive-table">  <tr>
	            <th>MOVIES LIST</th>
	          </tr>
	        </thead><tbody>

	<?php
	        $username = "z1803493";
	        $password = "1995Jul05";
//	        $username = "z1803493";
//	        $password = "1995Jul05";

	        try { 
	           $dsn = "mysql:host=Courses;dbname=z1803493";
//	            $dsn = "mysql:host=Courses;dbname=z1803493";

	            $pdo = new PDO($dsn, $username, $password);
	            
	            $sql = "select * from movie";
	            $stmt = $pdo->query($sql);
	            $result = $stmt -> fetchAll();
	            
	            foreach ( $result as $row) {
	                echo "<tr>";
	                echo "<td>",$row['title'],"</td>";
	            }
 ?>
	            </tbody></table>
	            <br>
<hr>
<blockquote> Second Question:  allow the user to choose a director or actor and list all of the movies that director or actor has been involved with and the total salary he/she earned  </blockquote>

<!-- 


-->

	<div class="row">
	
			<div class="input-field col s6">
    
	            <form action="movies.php"  method="post">

	        <?php

	            $dir = "select * from director";
	            $act = "select distinct * from actor";
	            $stmt_dir = $pdo->query($dir);
	            $stmt_act = $pdo->query($act);
	            $result_dir = $stmt_dir -> fetchAll();
	            $result_act = $stmt_act -> fetchAll();
	         ?>


	            <select name="select" required >

<option value="" disabled selected>Choose your option</option>
	         <?php
	            foreach ( $result_dir as $row ) {
	                echo '<option value="',$row[fname],' ',$row[lname],'">',$row[fname],' ',$row[lname],'</option>';
	            }


	            foreach ( $result_act as $row ) {
	                        echo '<option  value="',$row[fname],' ',$row[lname],'">',$row[fname],' ',$row[lname],'</option>';
	            }
?>
	            </select>
    <label> Director / Actor - * required</label>
  </div><button class="btn waves-effect waves-light" type="submit" name="action">Submit
    <i class="material-icons right">send</i>
  </button></div><form><br>
	       
<hr>
<blockquote> Third Question: list either all all directors with the movies they've directed or all of the actors with the movies they have been in. The movie title should be centered and bold with all of the items below it also centered, but not bold. (You may choose something other than bold, like a color change, or pick one other sql query that you think might be usefula size change)   </blockquote>
                <table class="highlight" class="centered" class="responsive-table">  <tr>
	            <th> DIRECTOR</th>
	                <th><CENTER> MOVIE DETAILS</CENTER></th>
	          </tr>

<!-- 


-->

	
	<?php
	            $sql = 'select   concat(director.fname," ",director.lname ) as FullName,movie.title, movie.length, movie.releaseYear   from directs join director on directs.did = director.did join movie on directs.mid = movie.mid ';
	            $stmt = $pdo->query($sql);
	            $result = $stmt -> fetchAll();
	            foreach ( $result as $row) {
	                echo "<tr>";
	                echo "<td>",$row['FullName'],"</td>";
	                
	                echo "<td><center><b> Title : ",$row['title'],"</b><br> Length : ",$row['length'],"<br> Release Year  :",$row['releaseYear'],"</center></td>";
	                
	                echo "</tr>";
	            }
	            echo "</table><br>";

	        }
	        catch(PDOexception $e) {
	            echo "Connection to database failed: " . $e->getMessage();
	        }
	    ?>
	    <hr>

	    <blockquote> Forth  Question:   pick one other sql query that you think might be useful </blockquote>


<!-- 
Its a simple query which shows all the data in te movies thought this is usefull ;)
-->

	       <table class="highlight" class="centered" class="responsive-table">  <tr>
	            <th>MOVIES TITLE</th>
	              <th>LENGTH</th>
	                <th>RELEASE YEAR</th>
	          </tr>
	        </thead><tbody>     
<?php	        
	            $sql = "select * from movie";
	            $stmt = $pdo->query($sql);
	            $result = $stmt -> fetchAll();
	           
	            foreach ( $result as $row) {
	                echo "<tr>";
	                echo "<td>",$row['title'],"</td>";
	                echo "<td>",$row['length'],"</td>";
	                echo "<td>",$row['releaseYear'],"</td>";
	                echo "</tr>";
	            }
	            echo "</table><br>";
	?>


	    </div>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
     
	<script src="js/materialize.min.js" type="text/javascript">
	</script> 
	<script type="text/javascript">
	       

	          $(document).ready(function() {
$('select').material_select();
  });
	             
	</script>
</body>
</html>