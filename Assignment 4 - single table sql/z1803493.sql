/*********************************************
CSCI 566 - Assignment 4 - Semester (Fall) 2016
Program:        z1803493.sql
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Jon Lehuta
Date Due:       Friday, September 30, 2016
**********************************************/
/* to use database */
use classicmodels;
/* 1 */
show tables;

/* 2a */
select data_type  
	from information_schema.columns 
	where table_name ="Customers";
/* 2b */
select data_type  
	from information_schema.columns 
	where table_name ="Orders";
/* 2c */
select data_type  
	from information_schema.columns 
	where table_name ="Products";
/* 3a */
select * from Products
	LIMIT 10;
/* 3b */
select productName, productCode, productDescription from Products
	LIMIT 05;
/* 4 */
select customerNumber, sum(amount) from Payments group by customerNumber order by sum(amount) desc limit 15;
/* 5 */
select count(customerNumber) from Customers;
/* 6 */
select count( distinct customerNumber)  from Orders;
/* 7 */
select officeCode, country, state,city   from Offices order by country,state ,city;
/* 8 */
select city from Offices order by city asc;
/* 9 */
select count(employeeNumber) from Employees;
/* 10 */
select officecode, count(employeeNumber) as numberofEmployees from Employees group by officeCode order by officeCode desc;
/* 11 */
select count(orderNumber) from Orders;
/* 12 */
select count(shippedDate) from Orders;
/* 13 */
select count(orderNumber), customerNumber from Orders group by customerNumber having count(orderNumber) > 5;
/* 14 */
select CONCAT(lastname, ', ',firstname) as Employee_Name from Employees;
/* 15 */
select * from Employees where officeCode ="7";

