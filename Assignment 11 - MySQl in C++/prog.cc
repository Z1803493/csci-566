
/************************************************
CSCI 466/566 - Assignment 11 - Semester (Fall) 2016
File:           prog.cc
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Jon Lehuta
TA:             Rajeswari Gundu
TA:             Bari Abdul
Date Due:       Monday, November 21, 2016
Purpose:        C++ program to connect to database 
                and display information 

*************************************************/

#include <mysql.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;
MYSQL *conn, mysql;
MYSQL_RES *res, *res2;
MYSQL_ROW row, row2;

int query_state, query_state2;

class Movie {
public:
    string movie_name, director_name, movie_id, director_id;
    vector<string> movie_actor;
    Movie()
        : movie_name("")
        , director_name("")
        , movie_id("")
        , director_id("")
    {
    }
    ~Movie() {}

    void print_details_movie()
    {
        cout << movie_name << endl
             << "\tDirected by : " << director_name << endl
             << "\t\tStarring : ";
        for (int i = 0; i < movie_actor.size(); ++i)
            cout << endl
                 << "\t\t\t" << movie_actor[i];
        cout << endl
             << endl;
    }
};

int main()
{
    vector<Movie> list_movies;

    const char* server = "Courses";
    const char* user = "z1803493";
    const char* password = "1995Jul05";
    const char* database = "z1803493";

    mysql_init(&mysql);
    conn = mysql_real_connect(&mysql, server, user, password, database, 0, 0, 0);

    if (!conn) {
        cout << mysql_error(&mysql) << endl;
        return -1;
    }

    string select_query = "select movie.title,  director.fname ,director.lname,  movie.mid , directs.did from movie join directs on movie.mid = directs.mid  join director on director.did = directs.did";
    query_state = mysql_query(conn, select_query.c_str());

    if (query_state) {
        cout << mysql_error(conn) << endl;
        return 1;
    }
    res = mysql_store_result(conn);
    while ((row = mysql_fetch_row(res)) != NULL) {

        vector<string> cast;
        Movie temp;
        stringstream ss, mname, dname, mid, did;
        mname << row[0];
        dname << row[1] << " " << row[2];
        mid << row[3];
        did << row[4];

        while (mname) {
            string lamo;
            mname >> lamo;
            temp.movie_name = temp.movie_name + lamo + " ";
        }
        while (dname) {
            string lamo;
            dname >> lamo;
            temp.director_name = temp.director_name + lamo + " ";
        }
        did >> temp.director_id;
        mid >> temp.movie_id;

        string query2 = "select performs_in.aid,actor.fname,actor.lname from performs_in join actor on performs_in.aid = actor.aid where performs_in.mid = " + temp.movie_id;
        query_state2 = mysql_query(conn, query2.c_str());
        if (query_state) {
            cout << mysql_error(conn) << endl;
            return 1;
        }
        res2 = mysql_store_result(conn);
        while ((row2 = mysql_fetch_row(res2)) != NULL) {
            stringstream ss;
            string control_string;
            ss << row2[1] << " " << row2[2];
            while (ss) {
                string lamo;
                ss >> lamo;
                control_string = control_string + lamo + " ";
            }
            temp.movie_actor.push_back(control_string);
        }
        list_movies.push_back(temp);
    }

    for (int i = 0; i < list_movies.size(); ++i)
        list_movies[i].print_details_movie();

    mysql_free_result(res);
    mysql_close(conn);
}
