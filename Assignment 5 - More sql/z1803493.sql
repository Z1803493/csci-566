/*********************************************
CSCI 566 - Assignment 4 - Semester (Fall) 2016
Program:        z1803493.sql
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Jon Lehuta
Date Due:       Tuesday, October 11, 2016
**********************************************/
use classicmodels
show tables;
describe Customers;
describe Offices;
describe Employees;
describe Orders;
describe OrderDetails;
describe Products;
/*1*/
select count(customerNumber) , city from Customers group by country;
/*2*/
select count(customerNumber) , city from Customers group by city;
/*3*/
select lastname,firstname, addressLine1, addressLine2, territory, city,state, country, postalCode from Employees inner join Offices on Offices.officeCode=Employees.officeCode ;
/*4*/
select orderNumber, sum(priceEach*quantityOrdered)from OrderDetails group by productCode;
/*5*/
select customerNumber,sum(priceEach*quantityOrdered)from OrderDetails inner join Orders on OrderDetails.orderNumber = Orders.orderNumber group by productCode limit 5 ;
/*6*/
select customerName, count(orderNumber) from Orders inner join Customers on Customers.customerNumber = Orders.customerNumber group by Orders.customerNumber;
/*7*/
select concat(e.firstName," ",e.lastName) as Employee_name , concat(em.firstName, " ",em.lastName) as report_name  , e.jobTitle from Employees e left join  Employees em on e.reportsTo=em.employeeNumber;
/*8*/
select Products.* , count(orderNumber) as no_of_orders, quantityOrdered as total_quantity from Products inner join OrderDetails on OrderDetails.productCode = Products.productCode;
/*9*/
select customerName, count(orderNumber) from Orders inner join Customers on Customers.customerNumber = Orders.customerNumber group by Orders.customerNumber order by count(OrderNumber) desc limit 5;
/*10*/
select customerName, concat(Employees.firstName," " ,Employees.lastName) as Employee_fullname from Customers inner join Employees on Customers.salesRepEmployeeNumber = Employees.employeeNumber ;



