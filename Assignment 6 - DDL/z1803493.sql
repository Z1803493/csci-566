/*********************************************
CSCI 566 - Assignment 6 - Semester (Fall) 2016
Program:        z1803493.sql
Author:         Patlolla, Akhil Reddy
Zid:            z1803493
Section:        1
Instructor:     Jon Lehuta
Date Due:       Monday, October 17, 2016
**********************************************/
#1
drop table account,person ;
drop view person_account;

#2
create table person (
	id int not null auto_increment,
	firstname varchar(100),
	lastname varchar(100),
	primary key (id)
);

#3
insert into person(firstname,lastname) values("akhil", "patlolla"),("akhil2", "patlolla2"),("akhil3", "patlolla3"),("akhil4", "patlolla4"),("akhil5", "patlolla5");

#4
select * from person;

#5
create index index_lastname ON person (lastname);

#6
create table account(
	account_id int not null auto_increment,
	account_type varchar(10) not null,
	balance float,
	person_id int,
	primary key (account_id),
	foreign key (person_id) references person(id)
);

#7
insert into account(account_type, balance, person_id) values("saving",400,1),("saving",900,2),("saving",800,3),("saving",1200,4),("checking",1800,4);


#8
select * from account;

#9
alter table person add zipcode int default '60115';


#10
select * from person;

#11
create view person_account as ( 
	select person.firstname, 
	person.lastname,
	account.account_type, 
	account.balance from person 
	inner join account on 
	person.id = account.person_id
);


#12
update account set balance = 0.9 * balance ;

#13
select * from person_account; 


