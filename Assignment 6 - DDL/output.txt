mysql> source z1803493.sql

1
Query OK, 0 rows affected (0.05 sec)
Query OK, 0 rows affected (0.00 sec)

2
Query OK, 0 rows affected (0.09 sec)

3
Query OK, 5 rows affected (0.02 sec)
Records: 5  Duplicates: 0  Warnings: 0

4
+----+-----------+-----------+
| id | firstname | lastname  |
+----+-----------+-----------+
|  1 | akhil     | patlolla  |
|  2 | akhil2    | patlolla2 |
|  3 | akhil3    | patlolla3 |
|  4 | akhil4    | patlolla4 |
|  5 | akhil5    | patlolla5 |
+----+-----------+-----------+
5 rows in set (0.00 sec)

5
Query OK, 0 rows affected (0.25 sec)
Records: 0  Duplicates: 0  Warnings: 0

6
Query OK, 0 rows affected (0.08 sec)

7
Query OK, 5 rows affected (0.02 sec)
Records: 5  Duplicates: 0  Warnings: 0

8
+------------+--------------+---------+-----------+
| account_id | account_type | balance | person_id |
+------------+--------------+---------+-----------+
|          1 | saving       |     400 |         1 |
|          2 | saving       |     900 |         2 |
|          3 | saving       |     800 |         3 |
|          4 | saving       |    1200 |         4 |
|          5 | checking     |    1800 |         4 |
+------------+--------------+---------+-----------+
5 rows in set (0.00 sec)

9
Query OK, 5 rows affected (0.25 sec)
Records: 5  Duplicates: 0  Warnings: 0

10
+----+-----------+-----------+---------+
| id | firstname | lastname  | zipcode |
+----+-----------+-----------+---------+
|  1 | akhil     | patlolla  |   60115 |
|  2 | akhil2    | patlolla2 |   60115 |
|  3 | akhil3    | patlolla3 |   60115 |
|  4 | akhil4    | patlolla4 |   60115 |
|  5 | akhil5    | patlolla5 |   60115 |
+----+-----------+-----------+---------+
5 rows in set (0.00 sec)

11
Query OK, 0 rows affected (0.10 sec)

12
Query OK, 5 rows affected (0.02 sec)
Rows matched: 5  Changed: 5  Warnings: 0

13
+-----------+-----------+--------------+---------+
| firstname | lastname  | account_type | balance |
+-----------+-----------+--------------+---------+
| akhil     | patlolla  | saving       |     360 |
| akhil2    | patlolla2 | saving       |     810 |
| akhil3    | patlolla3 | saving       |     720 |
| akhil4    | patlolla4 | saving       |    1080 |
| akhil4    | patlolla4 | checking     |    1620 |
+-----------+-----------+--------------+---------+
5 rows in set (0.00 sec)

mysql> 